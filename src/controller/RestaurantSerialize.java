package controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import model.Restaurant;

public class RestaurantSerialize {

	public static void Serialize(Restaurant restaurant) {
		try {
			FileOutputStream fileOut = new FileOutputStream("Restaurant.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(restaurant);
			out.close();
			fileOut.close();
		}catch(NotSerializableException exception) {
			System.out.println("Cannot serialize");
		} catch (IOException i) {
			i.printStackTrace();
		}
	}
	
	public static Restaurant Deserialize() {
		Restaurant r=null;
		try {
			FileInputStream fileIn = new FileInputStream("Restaurant.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			r = (Restaurant) in.readObject();
			in.close();
			fileIn.close();
			System.out.println(r);
			return r;
		} catch (IOException i) {
			System.out.println(i);
			 r = new Restaurant();
			 Serialize(r);
			 return r;
		} catch (ClassNotFoundException c) {

			System.out.println("Restaurant class not found");
			c.printStackTrace();
			return r = new Restaurant();
		}
	}
}
