package controller;

import java.io.Serializable;

import GUI.AdministratorGUI;
import GUI.ChefGUI;
import GUI.View;
import GUI.WaiterGUI;
import model.MenuItem;
import model.Restaurant;

public class MainControl implements Serializable{
	
	private AdministratorGUI admin;
	private ChefGUI chef = new ChefGUI("CHEF");
	private WaiterGUI waiter;
	private Restaurant restaurant = new Restaurant();
	private View mainProgram ;
	
	public void runApp()
	{
		MenuItem men = new MenuItem() {
			
			@Override
			public Double computePrice() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		men.setName("Chilli");
		men.setPrice(2.5);
		
		restaurant.createMenuItem(men);
		restaurant.addObserver(chef);
		mainProgram = new View(restaurant, admin, chef, waiter);
		restaurant=RestaurantSerialize.Deserialize();
		mainProgram.setVisible(true);
		mainProgram.run();
	}
	

}
