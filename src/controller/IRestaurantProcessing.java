package controller;

import java.io.IOException;

import java.util.ArrayList;

import model.MenuItem;
import model.Order;

public interface IRestaurantProcessing {

	/**
	 * 
	 * @param item item we want to create
	 * preCondition item != null
	 * postCondition list.size at preCond == list.size at post -1
	 */
	public void createMenuItem(MenuItem item);
	
	/**
	 * 
	 * @param item
	 *	preCondition item!=null
	 *  postCondition list.size = list.size at preCond +1
	 */
	public void deleteMenuItem(MenuItem item);
	
	/**
	 * 
	 * @param item
	 * preCondition item!=null
	 * postCondition item.pre.price != item.post.price
	 */
	public void editMenuItem(MenuItem item);
	
	/**
	 * 
	 * @param order
	 * @param menuItem
	 * preCondition order != null
	 * preCondition menuItem != null
	 * 
	 */
	public void createOrder(Order order, ArrayList<MenuItem> menuItem);
	
	
}
