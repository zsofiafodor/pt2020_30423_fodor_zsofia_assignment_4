package GUI;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import controller.GenerateIDs;
import controller.IRestaurantProcessing;
import model.MenuItem;
import model.Order;
import model.OrderDate;
import model.Restaurant;

public class WaiterGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton takeOrder = new JButton("Take order");
	private JButton showOrderedItems = new JButton("Show order");
	private JButton addItemToOrder = new JButton("Add");
	private JButton genBill = new JButton("Bill");
	private JButton listOrders = new JButton("All orders");
	private JFrame Waiter = new JFrame();
	private JPanel pane = new JPanel(new GridBagLayout());
	private JLabel tableNr = new JLabel("Table Number:");
	private JLabel date = new JLabel("Date: ");
	private JTextField writeDate = new JTextField(20);
	GridBagConstraints c = new GridBagConstraints();
	private JTextField writeTableNr = new JTextField(20);
	private JLabel menu = new JLabel("Menu");
	private JComboBox<MenuItem> showMenu = new JComboBox<MenuItem>();
	JTextArea chosenItems = new JTextArea();
	private GenerateIDs id = new GenerateIDs();
	ArrayList<Order> orderList = new ArrayList<Order>();
	private File bill;
	private Restaurant myres;
	ArrayList<MenuItem> copyList = null;
	protected double myprice;
	protected int myId;

	public WaiterGUI(String name, Restaurant res, File bill2) {
		// TODO Auto-generated constructor stub

		super(name);
		this.myres = res;
		this.bill =bill2;

		c.gridx = 0;
		c.gridy = 0;
		pane.add(tableNr, c);

		c.gridx = 2;
		c.gridy = 3;
		takeOrder.setPreferredSize(new Dimension(100, 20));
		pane.add(takeOrder, c);

		c.gridx = 2;
		c.gridy = 0;
		pane.add(writeTableNr, c);

		c.gridx = 0;
		c.gridy = 1;
		pane.add(date, c);

		c.gridx = 0;
		c.gridy = 2;
		pane.add(menu, c);

		c.gridx = 2;
		c.gridy = 2;
		pane.add(showMenu, c);

		c.gridx = 2;
		c.gridy = 1;
		pane.add(writeDate, c);

		c.gridx = 0;
		c.gridy = 3;
		addItemToOrder.setPreferredSize(new Dimension(70, 20));
		pane.add(addItemToOrder, c);

		c.gridx = 7;
		c.gridy = 0;
		genBill.setPreferredSize(new Dimension(100, 20));
		pane.add(genBill, c);

		c.gridx = 7;
		c.gridy = 1;
		listOrders.setPreferredSize(new Dimension(100, 20));
		pane.add(listOrders, c);

		c.gridx = 7;
		c.gridy = 2;
		showOrderedItems.setPreferredSize(new Dimension(100, 20));
		pane.add(showOrderedItems, c);

		this.add(pane);
		this.pack();
	}

	public void run() {

		ArrayList<MenuItem> list = myres.getMenu();
		Iterator<MenuItem> it = list.iterator();

		while (it.hasNext()) {
			MenuItem curentItem = it.next();
			System.out.println(curentItem.getName());
			showMenu.addItem(curentItem);
		}
		ArrayList<MenuItem> orderedItems = new ArrayList<MenuItem>();
		

		addItemToOrder.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MenuItem chosenItem = (MenuItem) showMenu.getSelectedItem();
				orderedItems.add(chosenItem);
			}
		});

		showOrderedItems.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame yourOrder = new JFrame("Your order");
				JTextArea chosenItems = new JTextArea();
				chosenItems.setBounds(230, 150, 100, 200);
				getContentPane().add(chosenItems);
				for (int i = 0; i < orderedItems.size(); i++) {
					chosenItems.append(orderedItems.get(i).toString());
					chosenItems.append("\n");

				}
				yourOrder.add(chosenItems);
				yourOrder.pack();
				yourOrder.setVisible(true);

			}
		});

		takeOrder.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				try {
					myprice = 0;
					int ID = id.getNextId();
					myId = ID;
					int tableNr = Integer.parseInt(writeTableNr.getText());
					String date = writeDate.getText();
					if (date.matches("[0-9][0-9].[0-9][0-9].[0-9][0-9][0-9][0-9]") != true) {
						JFrame frame = new JFrame();
						JOptionPane.showMessageDialog(frame, "Wrong date", "Warning", JOptionPane.WARNING_MESSAGE);
					}else {
					StringTokenizer myDate = new StringTokenizer(date, ".");
					int day = Integer.parseInt(myDate.nextToken());
					String month = GenerateMonth(Integer.parseInt(myDate.nextToken()));
					int year = Integer.parseInt(myDate.nextToken());

					OrderDate dateOfOrder = new OrderDate(day, month, year);
					Order order = new Order(ID, dateOfOrder, tableNr);

					copyList = new ArrayList<MenuItem>();

					Iterator<MenuItem> it = orderedItems.iterator();

					int i = 0;
					while (it.hasNext()) {
						i++;
						copyList.add(it.next());
					}

					if (i > 0) {
						createOrder(order, copyList);

						orderedItems.removeAll(orderedItems);
						chosenItems.setText("");

						orderList.add(order);
						myres.addOrder(order);
						

					} else {
						JFrame frame2 = new JFrame();
						JOptionPane.showMessageDialog(frame2, "Nothing ordered", "Warning",
								JOptionPane.WARNING_MESSAGE);
					}}
				} catch (Exception e1) {
					System.out.println(e);
				}

			}
		});

		listOrders.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				DefaultTableModel model = new DefaultTableModel();
				Map<Order, ArrayList<MenuItem>> myMap = myres.getMyMap();
				JFrame f = new JFrame();
				model.addColumn("ID");
				model.addColumn("Table");
				model.addColumn("Date");
				model.addColumn("Ordered Items");
				model.addColumn("Total price");
				double price = 0;
				String[] orders = new String[5];
				Iterator<Order> it = orderList.iterator();
				while (it.hasNext()) {
					Order currOrder = it.next();
					orders[0] = String.valueOf(currOrder.getID());
					orders[1] = String.valueOf(currOrder.getTableNr());
					orders[2] = currOrder.getDate().toString();

					ArrayList<MenuItem> orderItems = myMap.get(currOrder);
					StringBuilder myString = new StringBuilder();
					Iterator<MenuItem> it2 = orderItems.iterator();

					while (it2.hasNext()) {
						MenuItem currItem = it2.next();
						price += currItem.getPrice();
						myString.append(currItem.toString());
						myString.append(" ");
					}
					orders[3] = myString.toString();
					orders[4] = String.valueOf(price);
					model.addRow(orders);
					myprice = price;
					price = 0;
					

				}
				JTable table = new JTable(model);
				JScrollPane sp = new JScrollPane(table);
				f.add(sp);
				f.setSize(500, 200);
				f.setLocationRelativeTo(null);
				f.setVisible(true);
			}
		});
		
		genBill.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String id= String.valueOf(myId);
				String tableNr = writeTableNr.getText();
				String date = writeDate.getText();
				String totalPrice = String.valueOf(myprice);
			    BufferedWriter writer;
				try {
					
					writer = new BufferedWriter(new FileWriter(bill));
					
					writer.write(id + "\n" + "Table nr: "+tableNr + "\n" + "DATE: " + date + "\n" + "Price: " + totalPrice + "\n" + "Ordered items: ");
					for (int i = 0; i < orderedItems.size(); i++) {
						writer.write(orderedItems.get(i).toString() + "\n");
					}
					writer.newLine();
					writer.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			     
			    
				
			}
		});

	}
	
	

	public void createOrder(Order order, ArrayList<MenuItem> orderedItems) {
		// TODO Auto-generated method stub
		myres.createOrder(order, orderedItems);
	}

	public String GenerateMonth(int i) {

		if (i == 1)
			return "January";
		if (i == 2)
			return "February";
		if (i == 3)
			return "March";
		if (i == 4)
			return "April";
		if (i == 5)
			return "May";
		if (i == 6)
			return "June";
		if (i == 7)
			return "July";
		if (i == 8)
			return "August";
		if (i == 9)
			return "September";
		if (i == 10)
			return "October";
		if (i == 11)
			return "November";
		if (i == 12)
			return "December";
		return null;
	}

	
}
