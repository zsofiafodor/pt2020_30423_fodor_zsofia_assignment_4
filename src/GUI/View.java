package GUI;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import model.Restaurant;

public class View extends JFrame {

	private JButton admin = new JButton("Administrator");
	private JButton chef = new JButton("Chef");
	private JButton waiter = new JButton("Waiter");
	private static final long serialVersionUID = 1L;
	private JPanel pane = new JPanel(new GridBagLayout());
	private JFrame Frame = new JFrame();
	private File bill = new File("C:\\Users\\fodor\\OneDrive\\Documents\\EGYETEM\\YEAR2\\OOP\\eclipse-workspace\\PT2020_30423_Fodor_Zsofia_Assignment4\\bill.txt.txt");
	GridBagConstraints c = new GridBagConstraints();
	AdministratorGUI myad;
	ChefGUI myCh;
	WaiterGUI myW;
	Restaurant res;
	// Restaurant myRestaurant = new Restaurant();

	public View(Restaurant myRes, AdministratorGUI myAdmin, ChefGUI myChef, WaiterGUI myWaiter) {
		c.weightx = 0.5;
		c.weighty = 0.5;

		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.LINE_START;
		c.fill = GridBagConstraints.HORIZONTAL;

		pane.add(admin, c);
		c.gridy = 1;
		pane.add(chef, c);

		c.gridy = 2;
		pane.add(waiter, c);

		this.myad = myAdmin;
		this.myCh = myChef;
		this.res = myRes;
		this.myW = myWaiter;

		this.add(pane);
		this.pack();
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		this.setSize(200, 100);
	}

	public void run() {
		admin.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				myad = new AdministratorGUI("ADMIN", res);
				myad.run();
				myad.setLocationRelativeTo(null);
				myad.setSize(400, 200);
				myad.setVisible(true);

			}
		});

		chef.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				myCh.setVisible(true);
				myCh.setLocationRelativeTo(null);

			}
		});

		waiter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				WaiterGUI myWaiter = new WaiterGUI("WAITER", res, bill);
				myWaiter.run();
				myWaiter.setLocationRelativeTo(null);
				myWaiter.setSize(400, 200);
				myWaiter.setVisible(true);

			}
		});
	}
}
