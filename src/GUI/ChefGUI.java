package GUI;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;




public class ChefGUI extends JFrame implements Observer{

	private JLabel titleLabel;
	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	private static final long serialVersionUID = 1L;
	
	public ChefGUI(String string) {
		super(string);
		titleLabel = new JLabel("CHEF", SwingConstants.CENTER);
		pane.add(titleLabel);
		
		this.add(pane);
		this.setSize(200, 100);
	}
	
	
	@Override
	public void update(Observable o, Object arg) {
		this.setVisible(true);
		System.out.println(arg.toString());
		int x = JOptionPane.showConfirmDialog(null, arg, "Notification !", 2);
		if (x == 0) {
			JFrame frame = new JFrame();
			JOptionPane.showMessageDialog(frame ,"Chef will cook!");
			this.setVisible(false);
		} else {
			JFrame frame = new JFrame();
			JOptionPane.showMessageDialog(frame ,"Chef is busy, but will cook later!");
			this.setVisible(false);
		}
	}
	
	

}
