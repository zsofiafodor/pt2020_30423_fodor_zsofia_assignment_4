package GUI;

import java.awt.GridBagConstraints;


import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.NotSerializableException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import controller.IRestaurantProcessing;
import controller.RestaurantSerialize;
import model.BaseProduct;
import model.CompositeProduct;
import model.MenuItem;
import model.Order;
import model.Restaurant;

public class AdministratorGUI extends JFrame implements IRestaurantProcessing {

	private static final long serialVersionUID = 1L;
	private JPanel pane = new JPanel(new GridBagLayout());
	private JPanel secondFrame = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	private JButton button2 = new JButton("Create new menu item");
	private JButton button3 = new JButton("Delete menu item");
	private JButton button4 = new JButton("Edit menu item");
	private JButton button5 = new JButton("Show menu");
	private JButton createNewItem = new JButton("Create");
	private JLabel productName = new JLabel("Name:");
	private JLabel price = new JLabel("Price:");
	private JTextField writeName = new JTextField(20);
	private JTextField writePrice = new JTextField(20);
	private double overallPrice = 0;
	Restaurant res;

	public AdministratorGUI(String name, Restaurant restaurant) {

		super(name);
		this.res = restaurant;
		c.weightx = 0.5;
		c.weighty = 0.5;
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.LINE_START;
		c.fill = GridBagConstraints.HORIZONTAL;

		pane.add(button2, c);
		c.gridy = 1;
		pane.add(button3, c);

		c.gridy = 2;
		pane.add(button4, c);

		c.gridy = 3;
		pane.add(button5, c);

		this.add(pane);

	}

	public void run() {
		button2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				addItemToMenu(res);

			}

		});

		button3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				deleteItem();
			}
		});

		button5.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = new DefaultTableModel();
				model.addColumn("Name");
				model.addColumn("Price");
				JFrame f = new JFrame();
				if (!res.getMenu().isEmpty()) {
					ArrayList<MenuItem> myMenu = res.getMenu();

					Iterator<MenuItem> it = myMenu.iterator();
					while (it.hasNext()) {
						MenuItem temp = it.next();
						String[] rowData = new String[2];
						String name = temp.getName();
						Double price = temp.getPrice();
						rowData[0] = name;
						rowData[1] = Double.toString(price);
						model.addRow(rowData);
					}
				} else {
					String[] row = {};
					model.addRow(row);
				}
				JTable table = new JTable(model);

				JScrollPane sp = new JScrollPane(table);
				f.add(sp);
				f.setSize(500, 200);
				f.setLocationRelativeTo(null);
				f.setVisible(true);

			}
		});

		button4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editItem();

			}
		});

	}

	protected void editItem() {
		JPanel editItem = new JPanel(new GridBagLayout());
		JButton editIt = new JButton("Edit");
		JLabel newPrice = new JLabel("New Price:");
		JLabel name = new JLabel("Name:");
		JTextField editedItemName = new JTextField(20);
		JTextField editedItemPrice = new JTextField(20);

		c.gridx = 0;
		c.gridy = 0;
		editItem.add(name, c);

		c.gridx = 1;
		c.gridy = 0;
		editItem.add(editedItemName, c);

		c.gridx = 0;
		c.gridy = 1;
		editItem.add(newPrice, c);

		c.gridx = 1;
		c.gridy = 1;
		editItem.add(editedItemPrice, c);

		c.gridx = 1;
		c.gridy = 2;
		editItem.add(editIt, c);

		JFrame newFrame = new JFrame("Editing Item!");
		newFrame.add(editItem);
		newFrame.pack();
		newFrame.setLocationRelativeTo(null);
		newFrame.setVisible(true);

		editIt.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String name = editedItemName.getText();
				String newPrice = editedItemPrice.getText();

				Double price;

				price = Double.valueOf(newPrice);
				if (findByName(name) != null) {
					MenuItem editableItem = findByName(name);
					editableItem.setPrice(price);
					res.editMenuItem(editableItem);
					RestaurantSerialize.Serialize(res);
					newFrame.dispose();
				} else {
					JFrame frame2 = new JFrame();
					JOptionPane.showMessageDialog(frame2, "Can't edit products, only menuItems", "Warning",
							JOptionPane.WARNING_MESSAGE);
				}

			}
		});

	}

	protected void deleteItem() {
		JPanel deleteItem = new JPanel(new GridBagLayout());
		JButton deleteIt = new JButton("Delete");
		JLabel itemName = new JLabel("Name:");
		JTextField deletedItemName = new JTextField(20);

		c.gridx = 0;
		c.gridy = 0;
		deleteItem.add(itemName, c);

		c.gridx = 2;
		c.gridy = 0;
		deleteItem.add(deletedItemName, c);

		c.gridx = 1;
		c.gridy = 1;
		deleteItem.add(deleteIt, c);

		deleteIt.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String name = deletedItemName.getText();
				Integer baseprod = 0;
				Integer found = 0;
				if (findByName(name) != null) {
					res.deleteMenuItem(findByName(name));// if MenuItem
				} else {
					ArrayList<MenuItem> items = res.getMenu();
					ArrayList<BaseProduct> baseprods = res.getBaseProdList();
					for (int i = 0; i < baseprods.size(); i++) {
						BaseProduct curr = baseprods.get(i);
						if (curr.getName().equalsIgnoreCase(name)) {
							baseprod = 1;
							found = 1;
						}
					}
					if (baseprod == 1) {

						for (int j = 0; j < items.size(); j++) {
							MenuItem currItem = items.get(j);
							ArrayList<Object> productList = currItem.getMyproducts();
							for (int i = 0; i < productList.size(); i++) {
								if (productList.get(i) instanceof BaseProduct) {
									BaseProduct myprod = (BaseProduct) productList.get(i);
								
								if (myprod.getName().contentEquals(name)) {
									deleteMenuItem(items.get(j));
									RestaurantSerialize.Serialize(res);
									items.remove(j);
									found = 1;
								}}
							}

						}
					} else {

						ArrayList<CompositeProduct> compositeProds = res.getCompositeProdList();
						for (int i = 0; i < compositeProds.size(); i++) {
							if (compositeProds.get(i).getName().contentEquals(name)) {
								CompositeProduct prod = compositeProds.get(i);
								res.getCompositeProdList().remove(prod);
								compositeProds.remove(prod);
								found = 1;
							}
						}
					}

					if (found == 0) {

						JFrame frame2 = new JFrame();
						JOptionPane.showMessageDialog(frame2, "Item does not exist", "Warning",
								JOptionPane.WARNING_MESSAGE);
					}
				}

			}
		});
		JFrame newFrame = new JFrame("Deleting Item!");
		newFrame.add(deleteItem);
		newFrame.pack();
		newFrame.setLocationRelativeTo(null);
		newFrame.setVisible(true);

	}

	public MenuItem findByName(String name) {
		ArrayList<MenuItem> items = res.getMenu();
		Iterator<MenuItem> it = items.iterator();
		while (it.hasNext()) {
			MenuItem curentItem = it.next();
			if (curentItem.getName().compareTo(name) == 0)
				return curentItem;
		}
		return null;
	}

	public void addItemToMenu(Restaurant res) {

		c.gridx = 0;
		c.gridy = 1;
		secondFrame.add(productName, c);

		c.gridx = 2;
		c.gridy = 1;
		secondFrame.add(writeName, c);

		c.gridx = 0;
		c.gridy = 2;
		secondFrame.add(price, c);

		c.gridx = 2;
		c.gridy = 2;
		secondFrame.add(writePrice, c);

		c.gridy = 3;
		c.gridx = 2;
		secondFrame.add(createNewItem, c);
		JFrame secondFrame2 = new JFrame("Adding new item");
		secondFrame2.add(secondFrame);
		secondFrame2.pack();
		secondFrame2.setVisible(true);
		createNewItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String getPrice = writePrice.getText();
				if (!getPrice.isEmpty()) {
					if (getPrice.matches("[0-9]+")) {
						productType(getPrice, writeName.getText());
					} else {
						JOptionPane.showMessageDialog(secondFrame2, "Wrong price!!");
					}

				} else {

					JFrame addToMenu = new JFrame("Adding new item to menu");
					JPanel addingNewMenuItem = new JPanel(new GridBagLayout());
					String[] s = {};
					JComboBox<Object> menu = new JComboBox<Object>(s);
					JButton addItem = new JButton("Add Item");
					JButton finish = new JButton("Finish");
					c.gridx = 0;
					c.gridy = 3;
					addingNewMenuItem.add(addItem, c);
					c.gridy = 4;
					addingNewMenuItem.add(finish, c);
					ArrayList<BaseProduct> list = res.getBaseProdList();
					ArrayList<CompositeProduct> compList = res.getCompositeProdList();
					Iterator<BaseProduct> it = list.iterator();

					while (it.hasNext()) {
						MenuItem curentItem = it.next();
						menu.addItem(curentItem);
					}

					Iterator<CompositeProduct> it2 = compList.iterator();

					while (it2.hasNext()) {
						MenuItem curentItem = it2.next();
						menu.addItem(curentItem);
					}
					MenuItem myItem = new MenuItem() {

						@Override
						public Double computePrice() {
							return null;
						}
					};

					addItem.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {
							MenuItem selectedItem = (MenuItem) menu.getSelectedItem();
							overallPrice += selectedItem.getPrice();
							myItem.getMyproducts().add(selectedItem);
						}
					});

					finish.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {

							myItem.setName(writeName.getText());
							myItem.setPrice(overallPrice);

							createMenuItem(myItem);
							RestaurantSerialize.Serialize(res);
							overallPrice = 0;
							JPanel newItem = new JPanel(new GridBagLayout());
							JFrame myNewItem = new JFrame("Your new item");
							JLabel name = new JLabel(myItem.getName());
							JLabel price = new JLabel(myItem.getPrice().toString());
							JLabel name2 = new JLabel("Name: ");
							JLabel price2 = new JLabel("Price: ");
							c.gridx = 0;
							c.gridy = 0;
							newItem.add(name2, c);
							c.gridx = 1;
							newItem.add(name, c);
							c.gridx = 0;
							c.gridy = 1;
							newItem.add(price2, c);
							c.gridx = 1;
							newItem.add(price, c);

							myNewItem.add(newItem);
							myNewItem.pack();
							myNewItem.setLocationRelativeTo(null);
							myNewItem.setVisible(true);
							addToMenu.dispose();

						}
					});

					c.gridx = 0;
					c.gridy = 0;
					addingNewMenuItem.add(menu, c);
					addToMenu.add(addingNewMenuItem);
					addToMenu.pack();
					addToMenu.setLocationRelativeTo(null);
					addToMenu.setVisible(true);

				}
			}
		});

	}

	public void productType(String getPrice, String string) {
		JButton compositeProd = new JButton("Composite");
		JButton baseProd = new JButton("Base");
		JFrame chooseProductType = new JFrame("Please choose type of product");
		JPanel productType = new JPanel(new GridBagLayout());
		c.gridx = 0;
		c.gridy = 0;
		productType.add(compositeProd, c);

		c.gridx = 0;
		c.gridy = 1;
		productType.add(baseProd, c);

		chooseProductType.add(productType);
		chooseProductType.pack();
		chooseProductType.setLocationRelativeTo(null);
		chooseProductType.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		chooseProductType.setVisible(true);

		baseProd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				BaseProduct myprod = new BaseProduct(string, Double.valueOf(getPrice));
				res.newProduct(myprod);
				chooseProductType.dispose();
			}
		});

		compositeProd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				CompositeProduct comProd = new CompositeProduct(string, Double.valueOf(getPrice));
				res.newProduct(comProd);
				chooseProductType.dispose();
			}
		});
	}


	@Override
	public void createMenuItem(MenuItem item) {
		res.createMenuItem(item);
		
	}

	@Override
	public void deleteMenuItem(MenuItem item) {
		res.deleteMenuItem(item);

	}

	@Override
	public void editMenuItem(MenuItem item) {
		res.editMenuItem(item);

	}

	@Override
	public void createOrder(Order order, ArrayList<MenuItem> menuItem) {
		System.out.println("Admin can't take orders");
		
	}

}
