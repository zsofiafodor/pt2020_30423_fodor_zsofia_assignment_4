package model;

import java.awt.Container;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;

import controller.IRestaurantProcessing;

public class Restaurant extends Observable implements IRestaurantProcessing, Serializable {

	private ArrayList<MenuItem> Menu;
	private Map<Order, ArrayList<MenuItem>> myMap;
	private ArrayList<Order> myOrders;
	private  ArrayList<MenuItem> myItems = new ArrayList<MenuItem>();
	
	private ArrayList<BaseProduct> baseProdList = new ArrayList<BaseProduct>();
	private ArrayList<CompositeProduct> compositeProdList = new ArrayList<CompositeProduct>();
	
	
	public Restaurant() {
		Menu = new ArrayList<MenuItem>();
		myMap = new HashMap<Order, ArrayList<MenuItem>>();
		myOrders= new ArrayList<Order>();
		
	}
	public ArrayList<MenuItem> getMenu() {
		return Menu;
	}

	public void setMenu(ArrayList<MenuItem> menu) {
		Menu = menu;
	}

	public Map<Order, ArrayList<MenuItem>> getMyMap() {
		return myMap;
	}

	public void setMyMap(Map<Order, ArrayList<MenuItem>> myMap) {
		this.myMap = myMap;
	}

	public ArrayList<Order> getMyOrders() {
		return myOrders;
	}

	public void setMyOrders(ArrayList<Order> myOrders) {
		this.myOrders = myOrders;
	}

	

	public ArrayList<MenuItem> getMyItems() {
		return myItems;
	}
	public void setMyItems(ArrayList<MenuItem> myItems) {
		this.myItems = myItems;
	}
	
	
	public void addOrder(Order newOrder) {
		
		myOrders.add(newOrder);
		String details = new String();
		String endLine = "\n";
		details += "Added new order\n";
		details +=  "Ordered:";

		Map<Order, ArrayList<MenuItem>> Orders = getMyMap();
		ArrayList<MenuItem> list = Orders.get(newOrder);
		Iterator<MenuItem> it = list.iterator();
		while (it.hasNext()) {
			details += "  " + it.next().toString();
		}

		setChanged();
		notifyObservers(details);
	}

	@Override
	public void createOrder(Order order, ArrayList<MenuItem> menuItem) {
		assert order != null;
		assert menuItem != null;
		Order oldOrder = order;
		myMap.put(order, menuItem);
		assert oldOrder.equals(order);
	}

	@Override
	public void createMenuItem(MenuItem item) {

		assert item != null;
		int sizeBeforeInsert = Menu.size();
		Menu.add(item);
		int sizeAfterInsert = Menu.size();
		assert sizeBeforeInsert + 1 == sizeAfterInsert;

	}

	@Override
	public void deleteMenuItem(MenuItem item) {

		assert item != null;
		int preSize = Menu.size();
		Menu.remove(item);
		int postSize = Menu.size();
		assert preSize == postSize + 1;

	}

	@Override
	public void editMenuItem(MenuItem item) {
		assert item != null;
		Double postPrice = item.getPrice();
		Double prePrice = 0.0;

		String itemName = item.getName();
		Iterator<MenuItem> myIterator = Menu.iterator();
		while (myIterator.hasNext()) {
			
			MenuItem curr = myIterator.next();
			if (curr.getName() == itemName) {
				
				curr.setPrice(item.getPrice());
				prePrice = curr.getPrice();
			}
		}
		assert postPrice != prePrice;

	}

	
	public void newProduct(Object obj)
	{
		if(obj instanceof BaseProduct)
		{
			BaseProduct myprod = (BaseProduct) obj;
		
			baseProdList.add(myprod);
		}
		if(obj instanceof CompositeProduct)
		{
			CompositeProduct myprod = (CompositeProduct) obj;
			compositeProdList.add(myprod);
		}
	}

	public ArrayList<BaseProduct> getBaseProdList() {
		return baseProdList;
	}
	public void setBaseProdList(ArrayList<BaseProduct> baseProdList) {
		this.baseProdList = baseProdList;
	}
	public ArrayList<CompositeProduct> getCompositeProdList() {
		return compositeProdList;
	}
	public void setCompositeProdList(ArrayList<CompositeProduct> compositeProdList) {
		this.compositeProdList = compositeProdList;
	}
	
}
