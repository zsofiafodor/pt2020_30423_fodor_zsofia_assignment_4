package model;

import java.util.ArrayList;

public abstract class MenuItem implements java.io.Serializable{

	private ArrayList<Object> myproducts= new ArrayList<Object>();
	
	
	

	private static final long serialVersionUID = 1L;
	protected String name;
	protected Double price;
	
	public ArrayList<Object> getMyproducts() {
		return myproducts;
	}

	public void setMyproducts(ArrayList<Object> myproducts) {
		this.myproducts = myproducts;
	}
	
	public abstract Double computePrice();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	@Override
	public String toString()
	{
		return this.name + " "+this.price;
	}
	
}
