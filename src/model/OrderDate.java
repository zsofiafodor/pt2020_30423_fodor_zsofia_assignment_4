package model;

public class OrderDate {
	
	private int day;
	private String month;
	private int year;
	public OrderDate(int day2, String month2, int year2) {
		this.day = day2;
		this.month = month2;
		this.year = year2;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	@Override
	public String toString()
	{
		return this.getYear() + " " + this.getMonth() + " " + this.getDay();
	}

}
