package model;

public class Order {

	private int ID;
	private int tableNr;
	private OrderDate date;

	public Order(int iD2, OrderDate dateOfOrder, int tableNr2) {
		this.ID = iD2;
		this.date = dateOfOrder;
		this.tableNr = tableNr2;
	}

	

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getTableNr() {
		return tableNr;
	}

	public void setTableNr(int tableNr) {
		this.tableNr = tableNr;
	}

	public OrderDate getDate() {
		return date;
	}

	public void setDate(OrderDate date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		int hashcode = 0;
		hashcode = 3 * this.ID + 7 * this.tableNr + 13 * this.date.getDay();
		return hashcode;

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (obj == null || obj.getClass() != this.getClass())
			return false;

		Order newOrder = (Order) obj;

		return ((newOrder.ID == this.ID) && (newOrder.date.equals(this.date)) && (newOrder.tableNr == this.tableNr));
	}
}
